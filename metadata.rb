name              'training_mws_nginx'
maintainer        "NIBR IT"
maintainer_email  "devops@nebula.na.novartis.net"
license           "Apache 2.0"
description       "Installs and configures"
version           "0.0.1"
recipe            "training_mws_nginx","training_mws_nginx"

depends "mw-nginx"
depends "nibr-ssl"