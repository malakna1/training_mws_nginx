include_recipe "mw-nginx"

case node["deploy_env"]
when nil, ''
  env='dev'
when 'release'
  env='prod'
when 'test'
  env='test'
when 'prod'
  env='prod'
when 'ci'
  env='dev'
else
   env='dev'
end

#if  deploy_env == 'dev' then
#  nibr_ssl "app ssl for nginx" do
#       root_cert_source 'pki_trust'
#       project 'training_mws_nginx'
#       server_type 'nginx'
#       cid_env node["deploy_env"]
#  end
#end